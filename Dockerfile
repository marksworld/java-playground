# Use a base image that includes Java and Maven
FROM maven:3.8.4-openjdk-17-slim AS build

# Set the working directory in the container
WORKDIR /app

# Copy the application's source code into the container
COPY . .

# Build the application
# RUN mvn clean package

# Set the entrypoint to run the Spring Boot application
EXPOSE 8080
ENTRYPOINT mvn spring-boot:run