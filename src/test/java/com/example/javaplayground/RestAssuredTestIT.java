package com.example.javaplayground;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.greaterThan;

public class RestAssuredTestIT {
    @BeforeAll
    public static void setup(){
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    @DisplayName("Whatver")
    public void Whatever(){
        System.out.println(baseURI);
    }

    @Test
    @DisplayName("Student Api returns a 200 response from a GET request")
    public void GETRequest() {
        Response response = given()
                .contentType(ContentType.JSON)
                        .when()
                        .get("/api/student")
                        .then()
                        .extract().response();

        Assertions.assertEquals(200, response.statusCode());
    }

}


