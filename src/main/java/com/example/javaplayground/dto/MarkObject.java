package com.example.javaplayground.dto;

public class MarkObject {

    private String id;
    private Integer number;

    public MarkObject(String id, int number) {
        this.id = id;
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public Integer getNumber() {
        return number;
    }
}
