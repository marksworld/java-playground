package com.example.javaplayground.dto;

import com.example.javaplayground.dto.MarkObject;

public record MessageRequest(MarkObject message) {
}
