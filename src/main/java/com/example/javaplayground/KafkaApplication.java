/*package com.example.javaplayground;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class KafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaApplication.class, args);
	}
	@Bean
	CommandLineRunner commandLineRunner(KafkaTemplate<String, Object> kafkaTemplate){

		MarkObject variableNameMarkObject = new MarkObject(
				"markId",
				23
		);

		String mark = "mark";

		return args -> {
			//kafkaTemplate.send("mark-topic", "hello Kafka :)");
			kafkaTemplate.send("mark-topic", variableNameMarkObject);
		};
	}

}
*/