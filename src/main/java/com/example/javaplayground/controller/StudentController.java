package com.example.javaplayground.controller;


// API LAYER

import com.example.javaplayground.dto.Student;
import com.example.javaplayground.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    private final StudentService studentService;


    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @RequestMapping(path = "api/student")
    @GetMapping
    public List<Student> getStudent() {
        return studentService.getStudent();
    }

    @RequestMapping(path = "api/students")
    @GetMapping
    public List<Student> getStudents() {
        return studentService.getAllStudents();
    }
}
