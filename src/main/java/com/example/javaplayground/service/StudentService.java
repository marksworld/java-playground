package com.example.javaplayground.service;


import com.example.javaplayground.dto.Student;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

// SERVICE LAYER I.E. BUSINESS LOGIC
@Service
public class StudentService {
    public List<Student> getStudent() {
        return List.of(
                new Student(1L,
                        "Mark",
                        "mark@mark.com",
                        LocalDate.of(2000,12,12),
                        33
                )
        );
    }
    public List<Student> getAllStudents(){
        return List.of(
                new Student(1L,
                        "Mark",
                        "mark@mark.com",
                        LocalDate.of(2000,12,12),
                        33
                ),
                new Student(12L,
                        "Tom",
                        "tom@tom.com",
                        LocalDate.of(2000,12,12),
                        35
                )
        );
    }
}
