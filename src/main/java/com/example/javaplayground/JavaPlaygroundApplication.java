package com.example.javaplayground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class JavaPlaygroundApplication {

    public static void doesSomething(int number){
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i * j + "\t");
            }
            // Print a new line after each row
            System.out.println();
        }

        int[] numbers = {5, 2, 9, 1, 5, 6};

        String[] test = {"x","y","z"};



        System.out.println(test[1]);

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }


    }
    public static void main(String[] args) {

       // SpringApplication.run(JavaPlaygroundApplication.class, args);

        System.out.println("HELLO");
        doesSomething(2);
    }
}
