package com.example.javaplayground.spock

import spock.lang.Specification

class Test extends Specification {
    def "should be a simple test"(){
        expect:
        1==1
    }

    def "should demonstrate given-when-then"() {
        given:
        def shape = new Polygon(4)

        when:
        int sides = shape.getNumberOfSides()

        then:
        sides == 4
    }

    def "should expect exceptions"(){
        when:
        new Polygon(0)

        then:
        def e = thrown(TooFewSidesException)
        e.numberOfSides == 0
    }
}
