package com.example.javaplayground.spock

import com.example.javaplayground.JavaPlaygroundApplication
import com.example.javaplayground.dto.Student
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import spock.lang.Specification

import java.time.LocalDate

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@SpringBootTest(classes = [JavaPlaygroundApplication.class])
@AutoConfigureMockMvc
class StudentApiTests extends Specification {

    @Autowired
    private MockMvc mockMvc

    @Autowired
    private ObjectMapper mapper

    private static final Long id = 1L
    private static final studentName = "Mark"
    private static final email = "mark@mark.com"
    private static final dateOfBirth = LocalDate.of(2000,12,12)
    private static final age = 33

    def "GET call to the student endpoint returns student data"() {
        def expectedStudentResponse = new Student(id, studentName, email, dateOfBirth, age)

        given: "A valid call to the student endpoint"
        def response = getStudent().getResponse();

        when: "The status code response is returned"
        def statusCode = response.getStatus();

        then: "The status code is a 200 response"
        statusCode == 200;

        and: "The response body contains student response"
        String responseBody = response.getContentAsString();
        responseBody == mapper.writeValueAsString(List.of(expectedStudentResponse))
    }

    def "GET call to the students endpoint returns student data"() {
        given: "A valid call to the students get endpoint"
        def response = getStudents().getResponse();

        when: "The status code is returned"
        def statusCode = response.getStatus()

        then: "The status code is a 200 response"
        statusCode == 200;
    }

    private MvcResult getStudent() {
        MvcResult result = mockMvc.perform(get("/api/student")).andReturn()
        result
    }

    private MvcResult getStudents() {
        MvcResult result = mockMvc.perform(get("/api/students")).andReturn()
        result
    }
}
