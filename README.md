# java-playground
This is a java playground to get to grip with using kafka, groovy/spock for integration tests and spring for
some basic hardcoded api tests.

# run commands
mvn spring-boot:run - to run in terminal
mvn clean install - to run groovy tests

download kafka zookeeper 
run the kafka zookeeper commands below

# Locally run kafka with zookeeper
https://kafka.apache.org/quickstart

cluster id - 4YUqAIziRJW4Erw6QwRIXQ ?

# Kafka started locally using zookeeper:
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties

# Kafka connect on localhost:8083
bin/connect-distributed.sh config/connect-distributed.properties
localhost:8083/connectors

# To consume message from the kafka topic:
bin/kafka-console-consumer.sh --topic TOPIC_NAME --from-beginning --bootstrap-server localhost:9092
bin/kafka-console-consumer.sh --topic mark-topic --from-beginning --bootstrap-server localhost:9092

NEXT STEP:
Change the value data type - publish a custom object

THIS PROJECT CONTAINS:
Kafka
Groovy 
GRPC - in works

